use dblesson;

// ****
db.createCollection("contacts");

// ****
db.contacts.insertOne(

    {
    "nom":'Saliha',
    "dep":"info"
    }

);


// ****
db.contacts.insertOne(
{
    "nom":"Poitras",
    "dep":
    {"code":420, nom:"info"},
    "cours":"kba"
}

);

// ****
db.createCollection("programme");

// ****
db.programme.insertOne(
{"_id":21, "numad": 11, "nom":"Ruby" , "prenom":"Robin"});


// ****
db.Programmes.insertMany([
{ "_id":1, "numad": "11000", "nom" :"Patoche" ,"prenom":"Alain"},
{ "_id":2, "numad": "1200", "nom": "Patoche" ,"prenom":"Voila"},
{ "_id":3, "numad": "1300", "nom":"Lechat" ,"prenom":"Simba"}]);

db.Programmes.insertMany([
{ "_id":4 ,"numad": "2000", "nom":"Gable" ,"prenom":"Alain",
"programme": {"code":420,"nomprog":"info"}
},
{ "_id":5 ,"numad": "2000", "nom":"Leroy" ,"prenom": "Yanick",
"programme": {"code":410,"nomprog":"soins"}
},
]);


// ****
db.Programmes.find({"nom":"Patoche"});

db.Programmes.find({"nom":"Patoche", "prenom":"Alain"});

db.Programme.find({"nom":"Patoche"},{nom:1,prenom:1});

db.Programmes.find( { "programme": {"code":420, "nomprog":"info"}} ); 


// ****
db.employes.insertMany([
{ "_id":1, "numEmp": "1000", "nom" :"Dort" ,"prenom":"Celine","salaire":45000},
{ "_id":2, "numEmp": "1001", "nom": "Hind" ,"prenom":"Yo","salaire":20000},
{ "_id":3, "numEmp": "1002", "nom":"Lechat" ,"prenom":"Simba","salaire":52000}]);

db.employes.find();
db.employes.find({"salaire": {$gte:45000}});
db.employes.find( {"salaire": {$in:[45000,50000,35000]}});

// ****

db.Programmes.update(
    {"_id":11},
    {
        $inc:{"salaire":20},
    });
    
   
db.employes.update (
    {"_id":11},
    {
    $inc: {"salaire": 500}
    }
);


// ****

db.Programmes.update(
    {"_id":99},
    {
        "numad":"20",
        "nom":"Simpson",
        "prenom":"Bart"
    },
    
   {upsert:true} );
    
// ****
db.Programmes.remove({"_id":99});

db.Programmes.remove({"nom":"Ruba"});


// ****
db.Programmes.count();

db.Programmes.count({"nom":"Patoche"});

// ****


use MongoDBDriver;
use MongoDBBson;


