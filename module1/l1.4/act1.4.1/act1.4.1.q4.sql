INSERT INTO medicament(nom, descrpc, descrpl, condi)
VALUES ('Chourix', 'Médicamment contre la chute des choux', 'Vivamus fermentum semper porta.Nunc diam velit,adipiscing ut tristique vitae,sagittis vel odio. Maecenas convallis ullamcorper ultricies. Curabitur ornare',13);

INSERT INTO medicament(nom, descrpc, descrpl, condi)
VALUES ('Tropas', 'Médicament contre les dysfonctionnements intellectuels', 'Suspendisse lectus leo, consectetur in tempor sit amet, placerat quis neque. Etiam luctus porttitor lorem, sed suscipit est rutrum non',42);

INSERT INTO contreIndication(codec, desrip, nomMedicament)
VALUES ('CI1', 'Ne jamais prendre après minuit', 'Chourix');

INSERT INTO contreIndication(codec, desrip, nomMedicament)
VALUES ('CI2', 'Ne jamais mettre en contact avec de l eau', 'Chourix');

INSERT INTO contreIndication(codec, desrip, nomMedicament)
VALUES ('CI3', 'Garder à l abri de la lumière du soleil', 'Tropas');