use database_l1;

create table spectacle(
nospectacle integer primary key,
nom VARCHAR(50),
duree integer NOT NULL,
typ VARCHAR(50),
check (typ in ('theatre', 'danse', 'concert')),
check (duree between 30 and 90)
 );

create table salle(
nosalle integer primary key,
nbplaces integer,
check (nbplaces = 40));



