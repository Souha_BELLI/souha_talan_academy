use database_l1;

create table spectacle(
nospectacle integer primary key,
nom VARCHAR(50),
duree integer NOT NULL,
typ VARCHAR(50),
check (typ in ('theatre', 'danse', 'concert')),
check (duree between 30 and 90)
 );

create table salle(
nosalle integer primary key,
nbplaces integer,
check (nbplaces = 40));

#Question4: vérification des contraintes

Insert into spectacle(nospectacle,nom,duree,typ)
values (1,'hello', NULL, 'danse') ;
#Erreur car la duree doit être non nulle

Insert into spectacle(nospectacle,nom,duree,typ)
values (1,'hello', 15, 'danse') ;
#Erreur car la duree doit être entre  30 et 90

insert into salle(nosalle, nbplaces)
values (2, 30);
#Erreur car le nombre de places disponible dans la salle doit être égale à 40


