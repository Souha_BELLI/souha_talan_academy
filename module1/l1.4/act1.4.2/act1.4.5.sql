use database_l1;

create table producteur(
raisonSociale VARCHAR(100) primary key,
ville VARCHAR(100));

create table consommateur(
logi VARCHAR(100),
email VARCHAR(100),
nom VARCHAR(100) NOT NULL,
prenom VARCHAR(100) NOT NULL,
vill VARCHAR(100) NOT NULL,
primary key (logi, email),
UNIQUE (nom, prenom, vill));

create table produit(
id integer primary key,
descrip VARCHAR(100),
produitpar VARCHAR(100),
consommeparlog VARCHAR(100),
cosommeparemail VARCHAR(100),
foreign key (produitpar) references producteur(raisonSociale),
foreign key (consommeparlog) references consommateur(logi),
foreign key (cosommeparemail) references consommateur(email));


