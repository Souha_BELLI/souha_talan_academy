*	act1.5.1



-	Question1 : selectionner les employés ayant des revenus supérieurs à 10000 euros

R=Restriction(emp, (SAL+COMM)>10000)


-	Question2: trouver le nom et la profession de l'employé numéro 10

R=Restriction(EMP, ENO=10)
R'=projection(R, ENOM, PROF)

Question3: lister les noms des employés qui travaille à Paris
R=Jointure(DEPT,EMP, DNO=DNO)
R'=restriction(R, VILLE="paris")
R"=projection(R', ENOM)

-	Question4: trouver le nom du directeur du departement commercial

R=jointure(DEPT,EMP,DIR=ENO)
R'=Restriction(R, DNOM="commercial")
R"=projection(R',ENOM)

-	Question5: trouver les professions des directeurs des departements

R=jointure(DEPT,EMP, DIR=ENO)
R'=projection(R, ENOM, PROF)

-	Question6: trouver le nom des directeurs de departement ayant comme profession ingenieur

R=jointure(DEPT,EMP, DIR=ENO)
R'=restriction(R, PROF="ingénieur")
R"=projection(R', ENOM)




*	act1.5.2

Projection(jointure(R1, R2, R1.A1=R2.A1),R1.A1,R2.A2)

- Faux
- Vrai
- Vrai
- Faux



*	act1.5.3

Excercice f) :
l'opération relationnelle renvoie deux tuples.




*	act1.5.4

Exercice q) :

-	Question1: 
Intresection(R1, R2) = Différence (R1,Différence(R1,R2))

-	Question2:
Jointure(R1,R2, R1.A1=R2.A1) = restriction(produit(R1,R2), R1.A1=R2.A1)



Exercice faire du cinéma:

-	Question1: lister les films français(titre, année, réalisateur):

R=projection(FILMS, titre, année, réalisateur)


-	Question2: donner les années de sortie des films dans lesquels l'acteur Jean GABIN a joué

R=jointure(FILMS, ACTEURS, FILMS.titre= ACTEURS.titre)
R'=restriction(R, acteur="Jean GABIN")
R"=projection(R', année)


-	Question3:

R=jointure(FILMS, ACTEURS, FILMS.titre= ACTEURS.titre)
R'=restriction(R, R.réalisateur="François Truffaut")
R"=projection(R', R'.acteur)


-	Question4:

R=restriction(ACTEUR, acteur="Catherine Deneuve")

R'=jointure(R, ACTEUR, ACTEURS.titre=R.titre)

R"=différence(R',R)

R"'= projection(R", R".acteur)


-	Question5:

R=jointure(FILMS, ACTEURS, FILMS.titre= ACTEURS.titre)
R'=restriction(R, R.acteur=R.réalisateur)
R"=projection(R', R'.titre)


-	Question6:

R1=projection(FILMS, réalisateur)
R2=projection(ACTEURS,acteur)
R3=intersection(R1, R2)

R4=projection(R', R'.réalisateur) //R' de la question5
R5=Différence(R3, R4)


-	Question7:


R1=projection(FILMS, réalisateur)
R2=projection(ACTEURS,acteur)
R3=intersection(R1, R2)


-	Question8:

R=jointure(FILMS, ACTEURS, FILMS.titre= ACTEURS.titre)
R'=restriction(R, R.réalisateur="François Truffaut")
R"=projection(R', R'.acteur, R'.titre)

R3=division(R",ACTEURS)


*	act1.5.5

-	Exercice 1:

1-Vrai
2-Faux
3-Faux
4-Faux

-	Exercice 2:

1-Faux
2-Vrai
3-Faux
4-Faux
5-Vrai


-	Exercice 3:

R2d




*	act1.5.6


-	Question1:

R=Restriction(IMMEUBLE, NBETAGES>10 AND DATEC avant 01/01/1970)
R1=Projection(R, ADI)

-	Question2:

R=jointure(IMMEUBLE, PERSONNE, PROP=NOM)
R1=restriction(R, R.ADI=R.ADR)
R2=Projection(R1, R1.NOM)

-	Question3:

R3=Différence (R1,R2) // R1 et R2 de la question 2
R4=projection(R3, R3.NOM)

-	Question4:

R=jointure(IMMEUBLE, PERSONNE, PROP=NOM)
R1=restriction(R, R.ADI=R.ADR)
R2=restriction(R1, PROF="informaticien" AND AGE<40)
R3=projection(R2, R2.ADR)


-	Question5:


R=jointure(IMMEUBLE, PERSONNE, ADI=ADR)
R1=restriction(R, PROP="DUPONT")
R2=projection(R1, R1.NOM)


-	Question6:

R=jointure(IMMEUBLE, APPIM, IMMEUBLE.ADI=APPIM.ADI)
R1=restriction(R, R.OCCUP=NULL)
R2=projection(R1, R1.PROP)

R3=jointure(IMMEUBLE, PERSONNE, ADI=ADR)
R4=intersection(R2,R3)
R5=projection(R4, R4.NOM, R4.PROF)


-	Question7:









