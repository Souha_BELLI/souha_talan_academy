#Notion de références

#Question1
CREATE TABLE tuv(
pk_code CHAR(4) NOT NULL,
fk_etu CHAR(13) NOT NULL,
PRIMARY KEY(pk_code, fk_etu),
FOREIGN KEY(fk_etu) REFERENCES tetu(pk_numSecu));

INSERT INTO tuv(pk_code, fk_etu)
VALUES('NF17', '1800675001066');

INSERT INTO tuv(pk_code, fk_etu)
VALUES('NF26', '1800675001066');

INSERT INTO tuv(pk_code, fk_etu)
VALUES('NF29', '1800675001066');

#Question2
INSERT INTO tuv(pk_code, fk_etu)
VALUES('NF17', '2810592012232');

INSERT INTO tuv(pk_code, fk_etu)
VALUES('NF17', '1700792001278');

