#Projection, restriction et jointures

#Question1
SELECT nom, prenom
FROM tetu;

#Question2
SELECT *
FROM tetu
WHERE nom='Dupont';

#Question3
SELECT *
FROM tetu,tuv;

#Question4
SELECT *
FROM tetu JOIN tuv ON pk_numSecu=fk_etu;

#Question5
SELECT *
FROM tetu, tuv
WHERE pk_numSecu=fk_etu;
