#Fonctions et agrégats

#Question1
SELECT COUNT(pk_code)
FROM tuv;

#Question2
SELECT fk_etu, COUNT(pk_code)
FROM tuv
GROUP BY fk_etu;


#Question3
SELECT pk_code, COUNT(fk_etu)
FROM tuv
GROUP BY pk_code;