Use database_l1;
DESC tetu;
#NOTION DE TABLES
#Question1
CREATE TABLE tetu(
pk_numSecu CHAR(13) PRIMARY KEY,
k_numEtu VARCHAR(20) UNIQUE NOT NULL,
nom VARCHAR(50),
prenom VARCHAR(50));

#Question2
Use database_l1;
INSERT INTO tetu(pk_numSecu, k_numEtu, nom, prenom)
VALUES ('1800675001066', 'AB3937098X', 'Dupont', 'pierre');
INSERT INTO tetu(pk_numSecu, k_numEtu, nom, prenom)
VALUES ('2820475001124', 'XGB67668', 'Durand', 'Anne');

#Question3

SELECT pk_numSecu, k_numEtu, nom, prenom
FROM database_l1.tetu;

#Question4

SELECT nom, prenom
FROM database_l1.tetu
WHERE pk_numSecu='2820475001124'



