USE dbsouha;

DROP TABLE IF EXISTS `distrib`;

CREATE TABLE `distrib` (
  `id_distrib` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `cpostal` varchar(255) DEFAULT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `pays` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_distrib`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Dumping data for table `distrib`
--

LOCK TABLES `distrib` WRITE;
/*!40000 ALTER TABLE `distrib` DISABLE KEYS */;
INSERT INTO `distrib` VALUES (0,'gimages','0524509890',NULL,NULL,NULL,NULL),(1,'les films du losange','0172073438',NULL,NULL,NULL,NULL),(2,'mk2 diffusion','0165082653',NULL,NULL,NULL,NULL),(3,'rezo films','0503486847',NULL,NULL,NULL,NULL),(4,'studio images 5','0537834131',NULL,NULL,NULL,NULL),(5,'eiffel productions','0384390808',NULL,NULL,NULL,NULL),(6,'cerito films','0164444892',NULL,NULL,NULL,NULL),(7,'france 3 cin&amp;atilde;&amp;copy;ma','0387270953',NULL,NULL,NULL,NULL),(8,'tartan films','0267515944',NULL,NULL,NULL,NULL),(9,'monarchy enterprises b.v.','0134931386',NULL,NULL,NULL,NULL),(10,'advanced','0466014732',NULL,NULL,NULL,NULL),(11,'the vista organisation group','0437174826',NULL,NULL,NULL,NULL),(12,'les films balenciaga','0221824218',NULL,NULL,NULL,NULL),(13,'art-light productions','0356023625',NULL,NULL,NULL,NULL),(14,'telinor','0109372199',NULL,NULL,NULL,NULL),(15,'bandidos films','0371213306',NULL,NULL,NULL,NULL),(16,'parco co, ltd','0527672375',NULL,NULL,NULL,NULL),(17,'transfilm','0448268106',NULL,NULL,NULL,NULL),(18,'dmvb films','0215373495',NULL,NULL,NULL,NULL),(19,'davis-panzer productions','0535418285',NULL,NULL,NULL,NULL),(20,'idea productions','0313187914',NULL,NULL,NULL,NULL),(21,'vision international','0424465993',NULL,NULL,NULL,NULL),(22,'films a2','0243936488',NULL,NULL,NULL,NULL),(23,'dog eat dog productions','0216868187',NULL,NULL,NULL,NULL),(24,'the carousel pictures company','0578341887',NULL,NULL,NULL,NULL),(25,'interlight','0387722115',NULL,NULL,NULL,NULL),(26,'deluxe productions','0554695372',NULL,NULL,NULL,NULL),(27,'lolistar','0190887722',NULL,NULL,NULL,NULL),(28,'united international pictures (uip)','0511650332',NULL,NULL,NULL,NULL),(29,'verve pictures','0343139508',NULL,NULL,NULL,NULL),(30,'entertainment film distributors ltd','0136355344',NULL,NULL,NULL,NULL),(31,'eros film ltd.','0117308564',NULL,NULL,NULL,NULL),(32,'dogwoof pictures','0397752175',NULL,NULL,NULL,NULL),(33,'guerilla films ltd.','0126939575',NULL,NULL,NULL,NULL),(34,'ica films','0109521351',NULL,NULL,NULL,NULL),(35,'sony pictures','0117667343',NULL,NULL,NULL,NULL),(36,'20th century fox','0450273867',NULL,NULL,NULL,NULL),(37,'contender entertainment','0524345397',NULL,NULL,NULL,NULL),(38,'momentum pictures','0333654411',NULL,NULL,NULL,NULL),(39,'adlabs films','0255521648',NULL,NULL,NULL,NULL),(40,'artificial eye','0135718252',NULL,NULL,NULL,NULL),(41,'the works','0482435790',NULL,NULL,NULL,NULL),(42,'peccadillo pictures','0142367829',NULL,NULL,NULL,NULL),(43,'metrodome films','0184888460',NULL,NULL,NULL,NULL),(44,'icon film distribution uk','0285454608',NULL,NULL,NULL,NULL),(45,'bfi distribution','0278292428',NULL,NULL,NULL,NULL),(46,'optimum releasing','0210219708',NULL,NULL,NULL,NULL),(47,'miracle comms','0344573119',NULL,NULL,NULL,NULL),(48,'revolver entertainment','0489407327',NULL,NULL,NULL,NULL),(49,'soda pictures','0370402534',NULL,NULL,NULL,NULL),(50,'national film theater','0304269716',NULL,NULL,NULL,NULL),(51,'revelation films','0138006046',NULL,NULL,NULL,NULL),(52,'ace films','0588636787',NULL,NULL,NULL,NULL),(53,'path&amp;atilde;&amp;copy;','0530470806',NULL,NULL,NULL,NULL),(54,'v&amp;atilde;&amp;copy;rtigo films','0588492202',NULL,NULL,NULL,NULL),(55,'park circus','0517505479',NULL,NULL,NULL,NULL),(56,'buena vista international','0147740888',NULL,NULL,NULL,NULL),(57,'yeah yeah yeah ltd.','0579557407',NULL,NULL,NULL,NULL),(58,'swipe films','0438820671',NULL,NULL,NULL,NULL),(59,'universal','0118023807',NULL,NULL,NULL,NULL),(60,'paramount pictures','0586773629',NULL,NULL,NULL,NULL),(61,'warner bros u.k.','0379233026',NULL,NULL,NULL,NULL),(62,'showbox media group','0194205118',NULL,NULL,NULL,NULL),(63,'united pictures international uk','0398187864',NULL,NULL,NULL,NULL),(64,'paramount pictures uk','0127074098',NULL,NULL,NULL,NULL),(65,'buena vista international uk','0391010431',NULL,NULL,NULL,NULL),(66,'universal international pictures','0142892701',NULL,NULL,NULL,NULL),(67,'punk distribution','0387909616',NULL,NULL,NULL,NULL),(68,'axiom films','0115965167',NULL,NULL,NULL,NULL),(69,'eros international ltd.','0101316595',NULL,NULL,NULL,NULL),(70,'sony pictures uk','0576925615',NULL,NULL,NULL,NULL),(71,'lions gate films home entertainment','0486208894',NULL,NULL,NULL,NULL),(72,'studio 18','0311132263',NULL,NULL,NULL,NULL),(73,'british path&amp;atilde;&amp;copy;','0523400093',NULL,NULL,NULL,NULL),(74,'maiden voyage pictures','0355023753',NULL,NULL,NULL,NULL),(75,'warner music entertainment','0433185208',NULL,NULL,NULL,NULL),(76,'utv motion pictures','0334826167',NULL,NULL,NULL,NULL),(77,'lionsgate uk','0276522134',NULL,NULL,NULL,NULL),(78,'yume pictures','0309873282',NULL,NULL,NULL,NULL),(79,'delanic films','0412565948',NULL,NULL,NULL,NULL),(80,'vertigo films','0464391054',NULL,NULL,NULL,NULL),(81,'path&amp;atilde;&amp;copy; distribution ltd.','0267718795',NULL,NULL,NULL,NULL),(82,'spark pictures','0114385541',NULL,NULL,NULL,NULL),(83,'slingshot','0146903442',NULL,NULL,NULL,NULL),(84,'diffusion pictures','0448074755',NULL,NULL,NULL,NULL),(85,'transmedia pictures','0435146575',NULL,NULL,NULL,NULL),(86,'cinefilm','0139243944',NULL,NULL,NULL,NULL),(87,'odeon sky filmworks','0289544451',NULL,NULL,NULL,NULL),(88,'liberation entertainment','0537490904',NULL,NULL,NULL,NULL),(89,'lagoon entertainment','0462275200',NULL,NULL,NULL,NULL),(90,'halcyon pictures','0394022987',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `distrib` ENABLE KEYS */;
UNLOCK TABLES;

SELECT * FROM `distrib`;

#act1.6.16

SELECT REVERSE(substring(`telephone`,2)) AS enohpelet
FROM `distrib`
WHERE `telephone` LIKE '05%';

