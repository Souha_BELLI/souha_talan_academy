USE dbsouha;

SELECT `nom` AS NOM, MONTH(`date_abo`) AS MOISABONNEMENT, YEAR(`date_abo`) AS ANNEABONNEMENT
FROM `membre`
INNER JOIN `fiche_personne` ON `id_fiche_perso`=`id_perso`;