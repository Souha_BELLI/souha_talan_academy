USE dbsouha;

DROP TABLE IF EXISTS `historique_membre`;

CREATE TABLE `historique_membre` (
  `id_membre` int(11) NOT NULL,
  `id_film` int(11) NOT NULL,
  `date` datetime NOT NULL,
  KEY `id_membre` (`id_membre`),
  KEY `id_film` (`id_film`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historique_membre`
--

LOCK TABLES `historique_membre` WRITE;
/*!40000 ALTER TABLE `historique_membre` DISABLE KEYS */;
INSERT INTO `historique_membre` VALUES (14,378,'1999-03-01 00:00:00'),(14,395,'1999-03-12 00:00:00'),(14,381,'1999-03-03 00:00:00'),(14,380,'1999-02-24 00:00:00'),(14,397,'1999-05-03 00:00:00'),(14,382,'1999-02-06 00:00:00'),(14,387,'1999-02-16 00:00:00'),(18,391,'1999-04-06 00:00:00'),(18,343,'1998-12-24 00:00:00'),(18,375,'1999-02-19 00:00:00'),(18,376,'1999-02-07 00:00:00'),(18,374,'1999-01-28 00:00:00'),(20,290,'1998-09-22 00:00:00'),(20,389,'1999-03-17 00:00:00');
/*!40000 ALTER TABLE `historique_membre` ENABLE KEYS */;
UNLOCK TABLES;

INSERT INTO `historique_membre` VALUES (14,378,'1999-03-01 00:00:00'),(14,395,'1999-03-12 00:00:00'),(14,381,'1999-03-03 00:00:00');

#ACT1.6.17

SELECT * FROM `historique_membre`;

SELECT COUNT(`id_film`) AS FILMS
FROM `historique_membre`
WHERE `date` BETWEEN 30/10/2006 AND 27/07/2007;


SELECT YEAR(`date`) AS ANNEE , COUNT(`id_film`) AS FILMSNOEL
FROM `historique_membre`
WHERE `date` LIKE '24/12' AND (TIME(`date`) BETWEEN '18:00:00' AND '23:59:59') ; 



