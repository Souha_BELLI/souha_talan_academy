USE dbsouha;

DROP TABLE IF EXISTS `genre`;

CREATE TABLE `genre` (
  `id_genre` tinyint(4) NOT NULL,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id_genre`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (0,'detective'),(1,'dramatic comedy'),(2,'science fiction'),(3,'drama'),(4,'documentary'),(5,'animation'),(6,'comedy'),(7,'fantasy'),(8,'action'),(9,'thriller'),(10,'adventure'),(11,'various'),(12,'historical'),(13,'western'),(14,'romance'),(15,'music'),(16,'musical'),(17,'horror'),(18,'war'),(19,'unknow'),(20,'spying'),(21,'historical epic'),(22,'biography'),(23,'experimental'),(24,'short film'),(25,'erotic'),(26,'karate'),(27,'program'),(28,'family'),(29,'exp&amp;atilde;&amp;copy;rimental');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

#select * from `film` ORDER BY `id_genre`;


#act1.6.11


SELECT `titre`  AS `Titre`, `resum` AS `Resume`, `annee_prod`
FROM `film`
INNER JOIN `genre` ON `genre`.`id_genre`=`film`.`id_genre`
WHERE `genre`.`nom`='erotic'
order by `annee_prod` desc
