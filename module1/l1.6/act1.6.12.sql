USE dbsouha;

DROP TABLE IF EXISTS `abonnement`;

CREATE TABLE `abonnement` (
  `id_abo` int NOT NULL,
  `nom` varchar(255) NOT NULL,
  `resum` varchar(255) NOT NULL,
  `prix` int(11) NOT NULL,
  `duree_abo` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_abo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Dumping data for table `abonnement`
--

LOCK TABLES `abonnement` WRITE;
/*!40000 ALTER TABLE `abonnement` DISABLE KEYS */;
INSERT INTO `abonnement` VALUES (0,'VIP','l mois tout compris',60,30),(1,'GOLD','l\'abo pour les grosses l33t qui ont plein de sous',500,127),(2,'Classic','abonnement mensuel classique illimite',40,30),(3,'pass day','pass valable une journee',15,1),(4,'malsch','Le pass du malsch parcequ\'il le vau bien',238,4);
/*!40000 ALTER TABLE `abonnement` ENABLE KEYS */;
UNLOCK TABLES;

SELECT * FROM `abonnement`;

DROP TABLE IF EXISTS `membre`;

CREATE TABLE `membre` (
  `id_membre` int NOT NULL,
  `id_fiche_perso` int(11) NOT NULL,
  `id_abom` int NOT NULL,
  `date_abo` datetime DEFAULT NULL,
  `id_dernier_film` int(11) NOT NULL,
  `date_dernier_film` datetime DEFAULT NULL,
  `date_inscription` datetime DEFAULT NULL,
  PRIMARY KEY (`id_membre`),
  KEY `id_abom` (`id_abom`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `membre`
--

LOCK TABLES `membre` WRITE;
/*!40000 ALTER TABLE `membre` DISABLE KEYS */;
INSERT INTO `membre` VALUES (1,45,2,'2004-08-25 00:00:00',2154,'2004-09-12 00:00:00','2004-07-09 00:00:00'),(2,225,2,'2000-07-25 00:00:00',1411,'2002-08-12 00:00:00','2000-04-06 00:00:00'),(3,25,0,'2004-04-18 00:00:00',2482,'2005-06-27 00:00:00','2001-09-24 00:00:00'),(4,116,3,'1999-05-22 00:00:00',845,'2000-11-08 00:00:00','1999-04-15 00:00:00'),(5,224,4,'1999-08-29 00:00:00',1265,'2002-03-31 00:00:00','1999-07-14 00:00:00'),(6,32,2,'2004-12-16 00:00:00',3011,'2006-08-26 00:00:00','2004-10-17 00:00:00'),(7,7,2,'2006-05-18 00:00:00',3601,'2007-10-11 00:00:00','2005-05-10 00:00:00'),(8,160,1,'2003-12-25 00:00:00',2098,'2004-07-18 00:00:00','2001-10-02 00:00:00'),(9,44,2,'2002-11-21 00:00:00',2256,'2004-12-04 00:00:00','2002-11-21 00:00:00'),(10,61,2,'2004-06-28 00:00:00',3452,'2007-07-08 00:00:00','2004-04-06 00:00:00'),(11,122,2,'2003-07-21 00:00:00',2922,'2006-05-13 00:00:00','2002-08-07 00:00:00'),(12,191,4,'2007-03-24 00:00:00',3571,'2007-10-27 00:00:00','2005-05-05 00:00:00'),(13,101,2,'2006-04-03 00:00:00',2819,'2006-03-18 00:00:00','2005-10-19 00:00:00'),(14,169,0,'1999-05-25 00:00:00',544,'1999-11-14 00:00:00','1999-01-01 00:00:00'),(15,126,4,'2003-12-12 00:00:00',2025,'2004-04-04 00:00:00','2003-07-26 00:00:00'),(16,96,1,'2007-08-24 00:00:00',3567,'2007-10-23 00:00:00','2007-08-14 00:00:00'),(17,26,0,'2007-07-17 00:00:00',3615,'2007-11-12 00:00:00','2005-10-11 00:00:00'),(18,98,3,'2001-02-04 00:00:00',821,'2000-10-30 00:00:00','1998-10-02 00:00:00'),(19,27,2,'2006-05-30 00:00:00',3558,'2007-10-07 00:00:00','2004-12-03 00:00:00'),(20,173,0,'1998-09-04 00:00:00',640,'2000-02-23 00:00:00','1998-01-10 00:00:00'),(21,73,1,'2003-04-24 00:00:00',1724,'2003-06-12 00:00:00','2001-11-01 00:00:00'),(22,187,4,'2003-02-24 00:00:00',2107,'2004-07-25 00:00:00','2001-07-03 00:00:00'),(23,232,0,'2005-03-25 00:00:00',3595,'2007-10-30 00:00:00','2005-02-28 00:00:00'),(24,205,3,'2007-09-02 00:00:00',3583,'2007-11-02 00:00:00','2006-02-16 00:00:00'),(25,138,1,'2007-10-27 00:00:00',3602,'2007-11-09 00:00:00','2006-11-15 00:00:00'),(26,163,4,'1999-11-21 00:00:00',0,NULL,'1998-01-13 00:00:00'),(27,70,4,'2007-01-13 00:00:00',3599,'2007-10-11 00:00:00','2005-08-25 00:00:00'),(28,52,2,'2007-08-15 00:00:00',3575,'2007-11-08 00:00:00','2004-08-18 00:00:00'),(29,62,1,'2003-12-27 00:00:00',2659,'2005-11-02 00:00:00','2003-08-14 00:00:00'),(30,83,0,'2002-03-15 00:00:00',1456,'2002-09-25 00:00:00','1999-03-27 00:00:00');
/*!40000 ALTER TABLE `membre` ENABLE KEYS */;
UNLOCK TABLES;

SELECT * FROM `membre`;




#act1.6.12


SELECT UPPER(`fiche_personne`.`nom`) AS NOM, `prenom`, prix
FROM `membre`
INNER JOIN `fiche_personne` ON `fiche_personne`.`id_perso`=`membre`.`id_fiche_perso`
INNER JOIN `abonnement` ON `membre`.`id_abom`=`abonnement`.`id_abo`
WHERE prix>42
ORDER BY `fiche_personne`.`nom` , `prenom` ASC;


