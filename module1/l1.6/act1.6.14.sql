USE dbsouha;

DROP TABLE IF EXISTS `salle`;

CREATE TABLE `salle` (
  `id_salle` int(11) NOT NULL,
  `numero_salle` tinyint(4) NOT NULL,
  `nom_salle` varchar(255) NOT NULL,
  `etage_salle` int(11) NOT NULL,
  `nbr_siege` int(11) NOT NULL,
  PRIMARY KEY (`id_salle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


--
-- Dumping data for table `salle`
--

LOCK TABLES `salle` WRITE;
/*!40000 ALTER TABLE `salle` DISABLE KEYS */;
INSERT INTO `salle` VALUES (0,1,'pouet',0,135),(1,2,'machin',0,300),(2,3,'salle 3',0,85),(3,10,'caca',1,85),(4,11,'bidule',1,125),(5,12,'lapin',1,85),(6,13,'bouffon',1,300),(7,14,'toccard',1,85),(8,15,'malsch',1,280),(9,16,'boulet',1,125),(10,20,'pwet',2,200),(11,21,'mega salle',2,35),(12,22,'schtroumpf',2,89),(13,23,'bordel',2,225),(14,30,'barbe_b',3,225),(15,31,'smecta',3,38),(16,32,'bisacodyl',3,130);
/*!40000 ALTER TABLE `salle` ENABLE KEYS */;
UNLOCK TABLES;


SELECT * FROM `salle`;

#act1.6.14

SELECT ROUND(AVG(`nbr_siege`)) AS moyenne
FROM `salle`;

