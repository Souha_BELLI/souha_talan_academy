class Livre:

    def __init__(self, titre, auteur, prix, nbpage, genre=""):
        self.titre = titre
        self.auteur = auteur
        self.prix = prix
        self.nbpage = nbpage
        self.genre = genre

    def afficher(self, titre, auteur, prix, nbpage, genre=""):
        return titre, auteur, prix, nbpage, genre


class BandeDessinees(Livre):
    def __init__(self, titre, auteur, prix, nbpage, color, lecture="gauche à droite"):
        super().__init__(titre, auteur, prix, nbpage)
        self.color = color
        self.lecture = lecture

    def affiche(self, titre, auteur, prix, nbpage, color, lecture="gauche à droite"):
        return titre, auteur, prix, nbpage, color, lecture


class Manga(Livre):
    def __init__(self, titre, auteur, prix, nbpage, taille="petit", lecture="droite vers la gauche", color=False):
        super().__init__(titre, auteur, prix, nbpage)
        self.taille = taille
        self.lecture = lecture
        self.color = color

    def affiche(self, titre, auteur, prix, nbpage, taille="petit", lecture="droite vers la gauche", color=False):
        return titre, auteur, prix, nbpage, taille, lecture, color


class Roman(Livre):
    def __init__(self, titre, auteur, nbpage, prix, nbchap, resume):
        super().__init__(titre, auteur, prix, nbpage)
        self.nbchap = nbchap
        self.resume = resume

    def affiche(self, titre, auteur, nbpage, prix, nbchap, resume):
        return titre, auteur, nbpage, prix, nbchap, resume


class LivreRecette(Livre):
    def __init__(self, titre, auteur, prix, nbpage, recette=[]):
        super().__init__(titre, auteur, prix, nbpage)
        self.recette = recette

    def affiche(self, titre, auteur, prix, nbpage, recette=[]):
        return titre, auteur, prix, nbpage, recette

    def ajoutrecette(self,recette):
        self.recette.extend(recette)
        return self.titre, self.auteur, self.prix, self.nbpage, recette

class Recette(LivreRecette):
    def __init__(self, nom, description, niveau, liste="", astuce=""):
        self.nom = nom
        self.description = description
        self.niveau = niveau
        self.liste = liste
        self.astuce = astuce

    def ajoutastuce(self, astuce):
        self.astuce = astuce
        return self.nom, self.description, self.niveau, self.liste, astuce
    def ajoutliste(self, liste):
        self.list = liste
        return self.nom, self.description, self.niveau, liste, self.astuce

# lrc1

dt10 = ("Marmiton", "Philippe Etchebest", 15.98, 110)
lrc1 = LivreRecette(dt10[0], dt10[1], dt10[2], dt10[3])

# rc1

dt11 = ["Les pâtes crues", "Comment réaliser de délicieuses pâtes crues.", 3]
rc1 = Recette(dt11[0], dt11[1], dt11[2])

print("Instances crées")

# ajouter une astuce

x = "Ne pas les faire cuire."
print("l'astuce est ajoutée :")
print(rc1.ajoutastuce(x))


# ajouter une étape

y="Sortir les pâtes de leur emballage"
print("l'étape est ajoutée:")
print(rc1.ajoutliste(y))

# ajouter une recette à lrc1

dt11.extend(rc1.ajoutliste(y))
print("la recette est ajoutée:")
print(lrc1.ajoutrecette(dt11))