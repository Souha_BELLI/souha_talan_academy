class Livre:

    def __init__(self, titre, auteur, prix, nbpage, genre=""):
        self.titre = titre
        self.auteur = auteur
        self.prix = prix
        self.nbpage = nbpage
        self.genre = genre

    def afficher(self, titre, auteur, prix, nbpage, genre=""):
        return titre, auteur, prix, nbpage, genre


class BandeDessinees(Livre):
    def __init__(self, titre, auteur, prix, nbpage, color, lecture="gauche à droite"):
        super().__init__(titre, auteur, prix, nbpage)
        self.color = color
        self.lecture = lecture

    def affiche(self, titre, auteur, prix, nbpage, color, lecture="gauche à droite"):
        return titre, auteur, prix, nbpage, color, lecture


class Manga(Livre):
    def __init__(self, titre, auteur, prix, nbpage, taille="petit", lecture="droite vers la gauche", color=False):
        super().__init__(titre, auteur, prix, nbpage)
        self.taille = taille
        self.lecture = lecture
        self.color = color

    def affiche(self, titre, auteur, prix, nbpage, taille="petit", lecture="droite vers la gauche", color=False):
        return titre, auteur, prix, nbpage, taille, lecture, color


class Roman(Livre):
    def __init__(self, titre, auteur, nbpage, prix, nbchap, resume):
        super().__init__(titre, auteur, prix, nbpage)
        self.nbchap = nbchap
        self.resume = resume

    def affiche(self, titre, auteur, nbpage, prix, nbchap, resume):
        return titre, auteur, nbpage, prix, nbchap, resume

class LivreRecette(Livre):
    def __init__(self, titre, auteur, prix, nbpage, recette=[]):
        super().__init__(titre, auteur, prix, nbpage)
        self.recette = recette

    def affiche(self, titre, auteur, prix, nbpage, recette=[]):
        return titre, auteur, prix, nbpage, recette

    def ajoutrecette(self,recette):
        self.recette.extend(recette)
        return self.titre, self.auteur, self.prix, self.nbpage, recette

class Recette(LivreRecette):
    def __init__(self, nom, description, niveau, liste="", astuce=""):
        self.nom = nom
        self.description = description
        self.niveau = niveau
        self.liste = liste
        self.astuce = astuce

    def ajoutastuce(self, astuce):
        self.astuce = astuce
        return self.nom, self.description, self.niveau, self.liste, astuce
    def ajoutliste(self, liste):
        self.list = liste
        return self.nom, self.description, self.niveau, liste, self.astuce


# l1 et l2
dt1 = ("Le petit prince", "StExupéry", 10.40, 50)
l1 = Livre(dt1[0], dt1[1], dt1[2], dt1[3])
dt2 = ("Contes", "Grimm", 14.40, 254)
l2 = Livre(dt2[0], dt2[1], dt2[2], dt2[3])
print("Instances crées")

print("veuillez trouver l1")
print(l1.afficher(dt1[0], dt1[1], dt1[2], dt1[3]))
print("veuillez trouver l2")
print(l2.afficher(dt2[0], dt2[1], dt2[2], dt2[3]))

# b1 et b2

dt3 = ("Lucky Luke", "Morris", 10.40, 45, True)
b1 = BandeDessinees(dt3[0], dt3[1], dt3[2], dt3[3], dt3[4])
dt4 = ("Tintin", "Herge", 200.40, 45, False)
b2 = BandeDessinees(dt4[0], dt4[1], dt4[2], dt4[3], dt4[4])
print("Instances crées")

print("veuillez trouver b1")
print(b1.affiche(dt3[0], dt3[1], dt3[2], dt3[3], dt3[4]))
print("veuillez trouver b2")
print(b2.affiche(dt4[0], dt4[1], dt4[2], dt4[3], dt4[4]))

# m1 et m2

dt5 = ("One piece", "Eiichirō Oda", 5.40, 62)
m1 = Manga(dt5[0], dt5[1], dt5[2], dt5[3])
dt6 = ("One piece", "Eiichirō Oda", 5.40, 62)
m2 = Manga(dt6[0], dt6[1], dt6[2], dt6[3])
print("Instances crées")

print("veuillez trouver m1")
print(m1.affiche(dt5[0], dt5[1], dt5[2], dt5[3]))
print("veuillez trouver m2")
print(m2.affiche(dt6[0], dt6[1], dt6[2], dt6[3]))

# r1

dt9 = ("Dora", "Dora", 300, 3.5, 12, "Une description quelconque")
r1 = Roman(dt9[0], dt9[1], dt9[2], dt9[3], dt9[4], dt9[5])
print("Instances crées")

# lrc1

dt10 = ("Marmiton", "Philippe Etchebest", 15.98, 110)
lrc1 = LivreRecette(dt10[0], dt10[1], dt10[2], dt10[3])

# rc1

dt11 = ["Les pâtes crues", "Comment réaliser de délicieuses pâtes crues.", 3]
rc1 = Recette(dt11[0], dt11[1], dt11[2])

print("Instances crées")

# ajouter une astuce

x = "Ne pas les faire cuire."
print("l'astuce est ajoutée :")
print(rc1.ajoutastuce(x))


# ajouter une étape

y="Sortir les pâtes de leur emballage"
print("l'étape est ajoutée:")
print(rc1.ajoutliste(y))

# ajouter une recette à lrc1

dt11.extend(rc1.ajoutliste(y))
print("la recette est ajoutée:")
print(lrc1.ajoutrecette(dt11))
