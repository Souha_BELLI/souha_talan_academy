# class Compte
class Compte:
    code = 0
    solde = 0

    def __init__(self):
        self.solde = self.solde
        Compte.code += 1
        self.code = Compte.code

    def consult(self):
        self.solde = self.solde
        return f'le solde du compte {self.code} est : {self.solde:,.2f} €'.replace(",", " ")

    def creation(self, soldeintial=0):
        self.solde = soldeintial
        return self.code, self.solde

    def deposer(self, montant):
        self.montant = montant
        self.solde += montant
        return self.solde

    def retrait(self, montant):
        self.montant = montant
        self.solde -= montant
        return self.solde


# class CompteEpargne
class CompteEpargne(Compte):

    def calculInteret(self):
        self.solde = self.solde
        self.solde += 0.06 * self.solde
        return self.solde


# class ComptePayant
class ComptePayant(Compte):
    def retrait(self, montant):
        self.montant = montant
        self.solde -= montant + 2
        return self.solde


# instance des classes
compte = Compte()
print("le compte est crée")
print("le code et le solde intial respectivement sont: ", compte.creation())
compteepargne = CompteEpargne()
print("le compte épargne est crée")
print("le code et le solde intial respectivement sont: ", compteepargne.creation())
comptepayant = ComptePayant()
print("le compte payant est crée")
print("le code et le solde intial respectivement sont: ", comptepayant.creation())

# déposer une somme dans ces comptes

print("donner une somme à déposer dans les comptes")
x = input()

print("la somme a été ajoutée au compte 'compte', le solde est :", compte.deposer(int(x)))
print("la somme a été ajoutée au compte 'compteepargne', le solde est :", compteepargne.deposer(int(x)))
print("la somme a été ajoutée au compte 'comptepayant', le solde est :", comptepayant.deposer(int(x)))

# retirer une somme de ces comptes

print("donner une somme à retirer des comptes")
x = input()

print("la somme a été retirée du compte 'compte', le solde est :", compte.retrait(int(x)))
print("la somme a été retirée du compte 'compteepargne', le solde est :", compteepargne.retrait(int(x)))
print("la somme a été retirée du compte 'comptepayant', le solde est :", comptepayant.retrait(int(x)))

# faire appel au calcul interet

print("le calcul d'inetret est :", compteepargne.calculInteret())

# Afficher le solde des 3 comptes

print(compte.consult())
print(compteepargne.consult())
print(comptepayant.consult())
