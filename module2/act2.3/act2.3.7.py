
#1
a, b = 5, 2
b, a = a+1, b-a
print(a, b)

#résultat : -3 6

#2
a = 4
b = 2
b *= 3
a += b
print(a)

#résultat :10

a = b = 3
c = 2
b **= 2
b, c, a = a + c, a + b, b + c
print(a)