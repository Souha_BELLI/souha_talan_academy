print(str(4)*int("3")) #résultat : 444 ; l'instruction demande l'affichage du caractère 4 trois fois

print(int("3") + float("3.2")) #résultat :6.2 ; rendre le caractère "3" en entier et le multuplier par le réel 3.2

print(str(3) * float("3.2")) #résultat : erreur car on ne peut pas afficher un caractère 3.2 fois, le nombre doit être un entier

print(str(3/4)*2) #résultat :0.750.75 ; affichage du résultat de la division de 3. par deux fois