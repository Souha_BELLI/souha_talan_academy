# Pandas Getting Started
import pandas

mydataset = {
    'cars': ["BMW", "Volvo", "Ford"],
    'passings': [3, 7, 2]
}

myvar = pandas.DataFrame(mydataset)

print(myvar)

# ### pandas as pd

import pandas as pd

mydataset = {
    'cars': ["BMW", "Volvo", "Ford"],
    'passings': [3, 7, 2]
}

myvar = pd.DataFrame(mydataset)
print("\n panda as pd")
print(myvar)

# ### version

print("\n version")
print(pd.__version__)

# Pandas Series

a = [1, 7, 2]

myvar = pd.Series(a)
print("\n panda series")
print(myvar)

# ### labels
print("\n labels")
print(myvar[0])

# ### create labels
import pandas as pd

a = [1, 7, 2]

myvar = pd.Series(a, index=["x", "y", "z"])
print("\n create labels")
print(myvar)
print(myvar["y"])

# ### Key Value Objects as Series

calories = {"day1": 420, "day2": 380, "day3": 390}

myvar = pd.Series(calories)

print("\n Key Value Objects as Series")
print(myvar)

calories = {"day1": 420, "day2": 380, "day3": 390}

myvar = pd.Series(calories, index=["day1", "day2"])
print("\n index Key Value Objects as Series")
print(myvar)

# ### DataFrames

data = {
    "calories": [420, 380, 390],
    "duration": [50, 40, 45]
}

myvar = pd.DataFrame(data)
print("\n DataFrames")
print(myvar)

# ### Pandas DataFrames

data = {
    "calories": [420, 380, 390],
    "duration": [50, 40, 45]
}

# ### load data into a DataFrame object:
df = pd.DataFrame(data)

print("\n Pandas DataFrames")
print(df)

# ### refer to the row index:
print("\n refer to the row index")
print(df.loc[0])

# ### use a list of indexes:
print("\n use a list of indexes:")
print(df.loc[[0, 1]])

data = {
  "calories": [420, 380, 390],
  "duration": [50, 40, 45]
}

df = pd.DataFrame(data, index = ["day1", "day2", "day3"])
print("\n use a list of indexes")
print(df)

# refer to the named index:
print("\nLocate Named Indexes")
print(df.loc["day2"])

# Pandas Read CSV

df = pd.read_csv(r"C:\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\data.csv")
print("\nLoad Files Into a DataFrame")
print(df)

# ### print all the data
df = pd.read_csv('data.csv')
print("print all the data")
print(df.to_string())

# ### max_rows
print("max_rows")
print(pd.options.display.max_rows)

# ### Increase the maximum number of rows to display the entire DataFrame

pd.options.display.max_rows = 9999
df = pd.read_csv('data.csv')
print("Increase the maximum number of rows")
print(df)

# Pandas Read JSON

df = pd.read_json("https://www.w3schools.com/python/pandas/data.js")
print("Load the JSON file into a DataFrame")
print(df.to_string())

# ### Load a Python Dictionary into a DataFrame
data = {
  "Duration":{
    "0":60,
    "1":60,
    "2":60,
    "3":45,
    "4":45,
    "5":60
  },
  "Pulse":{
    "0":110,
    "1":117,
    "2":103,
    "3":109,
    "4":117,
    "5":102
  },
  "Maxpulse":{
    "0":130,
    "1":145,
    "2":135,
    "3":175,
    "4":148,
    "5":127
  },
  "Calories":{
    "0":409,
    "1":479,
    "2":340,
    "3":282,
    "4":406,
    "5":300
  }
}

df = pd.DataFrame(data)
print("\nLoad a Python Dictionary into a DataFrame")
print(df)

# Pandas - Analyzing DataFrames
df = pd.read_csv('data.csv')
print("Viewing the Data")
print(df.head(10))

# ### Print the first 5 rows of the DataFrame

df = pd.read_csv('data.csv')
print("\nPrint the first 5 rows of the DataFrame")
print(df.head())

# ### Print the last 5 rows of the DataFrame:
print("\nPrint the last 5 rows of the DataFrame:")
print(df.tail())

# ###Info About the Data
print("Info About the Data")
print(df.info())

# Pandas - Cleaning Data
df = pd.read_csv(r'C:\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
# ### Return a new Data Frame with no empty cells
new_df = df.dropna()
print("\nReturn a new Data Frame with no empty cells")
print(new_df.to_string())

# ### Remove all rows with NULL values:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
df.dropna(inplace = True)
print("\nRemove all rows with NULL values:")
print(df.to_string())

# ### Replace NULL values with the number 130:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
df.fillna(130, inplace = True)
print("\nReplace NULL values with the number 130:")
print(df.to_string())

# ### Replace NULL values in the "Calories" columns with the number 130:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
df["Calories"].fillna(130, inplace = True)
print("\nReplace NULL values in the Calories columns with the number 130:")
print(df.to_string())

# ### Calculate the MEAN, and replace any empty values with it:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
x = df["Calories"].mean()

df["Calories"].fillna(x, inplace = True)
print("\nCalculate the MEAN, and replace any empty values with it:")
print(df.to_string())

# ### Calculate the MEDIAN, and replace any empty values with it:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
x = df["Calories"].median()
df["Calories"].fillna(x, inplace = True)
print("\nCalculate the MEDIAN, and replace any empty values with it:")
print(df.to_string())


# ### Calculate the MODE, and replace any empty values with it:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
x = df["Calories"].mode()[0]
df["Calories"].fillna(x, inplace = True)
print("\nCalculate the MODE, and replace any empty values with it:")
print(df.to_string())

# ### Convert to date:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
df['Date'] = pd.to_datetime(df['Date'])
print("\nConvert to date:")
print(df.to_string())

# ### Remove rows with a NULL value in the "Date" column:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
df['Date'] = pd.to_datetime(df['Date'])
df.dropna(subset=['Date'], inplace = True)
print("\nRemove rows with a NULL value in the Date column:")
print(df.to_string())

# Pandas - Fixing Wrong Data

# ### Set "Duration" = 45 in row 7:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')

df.loc[7, 'Duration'] = 45
print("\nSet Duration = 45 in row 7:")
print(df.to_string())

# ### Loop through all values in the "Duration" column
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
for x in df.index:
  if df.loc[x, "Duration"] > 120:
    df.loc[x, "Duration"] = 120

print("\nLoop through all values in the Duration column.")
print(df.to_string())

# ### Delete rows where "Duration" is higher than 120:
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
for x in df.index:
  if df.loc[x, "Duration"] > 120:
    df.drop(x, inplace = True)

print("\nDelete rows where Duration is higher than 120:")
print(df.to_string())

# Pandas - Removing Duplicates

df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
print("\nDuplicates")
print(df.duplicated())

# ### Removing Duplicates
df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\dirtydata.csv')
df.drop_duplicates(inplace = True)
print("\nRemoving Duplicates")
print(df.to_string())
