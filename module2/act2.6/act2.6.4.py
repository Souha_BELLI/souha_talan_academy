import pandas as pd

df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\Chain_replacement.csv')

for x in df.index:
    if len(str(df.loc[x, "REPLACING-RENAULT-REF"])) != 10 or (str(x)).isdigit() is False:
        df.drop(x, inplace=True)

for x in df.index:

    if str(df.loc[x, "REPLACING-SUPPLIER-NAME"]) in ["aws", "AWS", "Aws"]:
        df.drop(x, inplace=True)


print("shipping date")
for x in df.index:
    if df.loc[x, "REPLACED-SUPPLIER-REF"] == "CONSOMMABLES":
        df.loc[x, "SHIPPING_DATE"] = "2022"
    elif df.loc[x, "REPLACED-SUPPLIER-REF"] == "NOUVEAU":
        df.loc[x, "SHIPPING_DATE"] = "2023"
    else:
        df.loc[x, "SHIPPING_DATE"] = "2024"


print("sorting")
df['REPLACEMENT-DATE'] = pd.to_datetime(df['REPLACEMENT-DATE'], errors='coerce')

df.dropna(subset=['REPLACEMENT-DATE'], inplace=True)

df.sort_values(["REPLACEMENT-DATE"],
                    axis=0,
                    ascending=True,
                    inplace=True)


df.drop_duplicates("REPLACING-RENAULT-REF", inplace = True,)

df.fillna("Empty", inplace = True)

print("execution is done")
print(df.to_string())


df.to_csv('chain_replacement_clean.csv')

print("fichier chain_replacement_clean rempli ")