mydict = {'prénon': 'Alex', 'nom': 'Boreau','ville': 'Paris', 'Classe': 'c1'}
print(mydict)

#ajouter au dictionnaire
mydict['Email'] ='alex.boreau@gmail.com'
mydict['Telephone']='21650236569'
print(mydict)

# récupérer et afficher la valeur de “ville”
print(mydict.get('ville'))

#supprimer la clé “ville” du dictionnaire
del mydict['ville']
print(mydict)

#fusionner les deux dictionaires
new_dict={'k1':'V1', 'k2':'V2'}

mydict.update(new_dict)
print(mydict)