liste=[17, 38, 10, 25, 72]
print(liste)

#Compter le nombre d'items de la liste
len(liste)

#ajoutez l’élément 12 à la liste et affichez la liste
liste.append(12)
print(liste)

#renversez et affichez la liste
liste.reverse()
print(liste)

#affichez l’indice de l’élément 17
print(liste.index(17))

#afficher l’element de l’indice 3
print(liste[3])

#enlevez l’élément 38 et affichez la liste
liste.remove(38)
print(liste)

#affichez le dernier élément de la liste
print(liste[-1])

#Trouver l'index de la valeur 10
print(liste.index(10))

#Afficher les 3 dernières occurrences
print(liste[-3:])