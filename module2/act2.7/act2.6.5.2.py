import mysql.connector

mydb = mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    password="Newstart1234.",
    database="mydatabase"
)

#drop tables
mycursor = mydb.cursor()
sql = "DROP TABLE FILMS"
mycursor.execute(sql)

mycursor = mydb.cursor()
sql = "DROP TABLE ACTEURS"
mycursor.execute(sql)


# create new tables

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE FILMS (IDFILM INT AUTO_INCREMENT PRIMARY KEY, TITRE VARCHAR(50),PAYS VARCHAR(10), "
                 "ANNEE INT(4),REALISATEUR VARCHAR(20), DUREE int(3))")

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE ACTEURS (IDACTEUR INT AUTO_INCREMENT PRIMARY KEY, TITRE VARCHAR(50),ACTEUR VARCHAR(20))")

# insertion des données

mycursor = mydb.cursor()

sql = "INSERT INTO FILMS (TITRE,PAYS,ANNEE, REALISATEUR, DUREE) VALUES (%s, %s, %s, %s, %s)"
val = [
  ('Bonjour', 'France', 1999, "François Truffaut", 100),
  ('Hello', 'Inde', 2002, "Raj", 300),
  ('lire', 'France', 2012, "Marco", 20),
  ('Fleur', 'France', 2020, "Max", 30),
  ('passé', 'France', 1998, "François Truffaut", 110),

]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")


mycursor = mydb.cursor()

sql = "INSERT INTO ACTEURS (TITRE,ACTEUR) VALUES (%s, %s)"
val = [
  ('Bonjour', 'Lin'),
  ('Hello', 'Catherin Deneuve'),
  ('lire', 'Jean GABIN'),
  ('Fleur', 'Jean GABIN'),
  ('Bonjour', 'steph'),
  ('Bonjour', 'Will'),
  ('Hello', 'Hin'),
  ('Hello', 'Bill'),
  ('Hello', 'Raj'),
  ('lire', 'Max'),
  ('passé', 'Max'),
  ('passé', 'Lin'),

]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

# lister les films français

mycursor = mydb.cursor()

sql = "SELECT TITRE, ANNEE, REALISATEUR FROM FILMS WHERE PAYS= 'France' "

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# donner les annee de sortie des films dont jean GABIN est acteur

mycursor = mydb.cursor()

sql = 'SELECT \
  FILMS.TITRE AS TITRE \
  FILMS.ANNEE AS ANNEE \
  ACTEURS.ACTEUR AS ACTEUR,\
  FROM FILMS \
  INNER JOIN ACTEURS ON FILMS.TITRE = ACTEURS.TITRE \
  WHERE ACTEUR="Jean GABIN"'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# trouver les acteurs qui ont tourné avec François Truffaut
mycursor = mydb.cursor()

sql = 'SELECT \
  FILMS.TITRE AS TITRE, \
  ACTEURS.ACTEUR AS ACTEUR\
  FROM FILMS \
  INNER JOIN ACTEURS ON FILMS.TITRE = ACTEURS.TITRE \
  WHERE REALISATEUR="François Truffaut"'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# trouver les acteurs qui ont été partenaire avec Catherin Deneuve

mycursor = mydb.cursor()

sql = 'SELECT FILMS.TITRE AS TITRE,  \
      ACTEURS.ACTEUR AS ACTEUR  \
      FROM FILMS INNER JOIN ACTEURS ON FILMS.TITRE = ACTEURS.TITRE  \
      WHERE ACTEUR <> "Catherin Deneuve" AND ACTEURS.TITRE in ( SELECT TITRE FROM ACTEURS WHERE ACTEURS.ACTEUR="Catherin Deneuve")'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

# lister les films dans lesquels le réalisateur est aussi acteur
mycursor = mydb.cursor()

sql = 'SELECT \
  FILMS.TITRE AS TITRE, \
  FILMS.REALISATEUR AS REALISATEUR,\
  ACTEURS.ACTEUR AS ACTEUR\
  FROM FILMS \
  INNER JOIN ACTEURS ON FILMS.TITRE = ACTEURS.TITRE \
  WHERE REALISATEUR=ACTEUR'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

# lister les réalisateurs n'ayant joué comme acteurs que dans des films qu'ils ne réalisaient pas eux mêmes
mycursor = mydb.cursor()

sql = 'SELECT FILMS.TITRE AS TITREF,  \
      ACTEURS.TITRE AS TITRE ,  \
      FILMS.REALISATEUR AS REALISATEUR,  \
      ACTEURS.ACTEUR AS ACTEUR \
      FROM FILMS \
      INNER JOIN ACTEURS ON FILMS.REALISATEUR = ACTEURS.ACTEUR  \
      WHERE FILMS.TITRE <> ACTEURS.TITRE'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# lister les réalisateurs n'ayant joué comme acteurs dans des fils qu'ils ne réalisaient pas eux mêmes

mycursor = mydb.cursor()

sql = 'SELECT FILMS.TITRE AS TITREF,  \
      ACTEURS.TITRE AS TITRE ,  \
      FILMS.REALISATEUR AS REALISATEUR,  \
      ACTEURS.ACTEUR AS ACTEUR \
      FROM FILMS \
      INNER JOIN ACTEURS ON FILMS.REALISATEUR = ACTEURS.ACTEUR'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

# donner les acteurs qui jouent dans tous les films de François Truffaut

mycursor = mydb.cursor()

sql = 'SELECT FILMS.TITRE AS TITREF \
      , ACTEURS.TITRE AS TITRE ,  \
      FILMS.REALISATEUR AS REALISATEUR,  \
      ACTEURS.ACTEUR AS ACTEUR  \
      FROM FILMS INNER JOIN ACTEURS ON FILMS.TITRE = ACTEURS.TITRE  \
      WHERE REALISATEUR="François Truffaut" '

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)