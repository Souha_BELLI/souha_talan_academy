import mysql.connector

mydb = mysql.connector.connect(
  host="127.0.0.1",
  user="root",
  password="Newstart1234.",
  database="mydatabase"
)

# create new tables
mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE emp2 (eno INT AUTO_INCREMENT PRIMARY KEY,enom VARCHAR(255), prof VARCHAR(255), sal float, comm float,dno int)")

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE dept2 (dno INT AUTO_INCREMENT PRIMARY KEY,dnom VARCHAR(255), dir int,ville VARCHAR(255))")


#drop tables
mycursor = mydb.cursor()
sql = "DROP TABLE emp"
mycursor.execute(sql)

mycursor = mydb.cursor()
sql = "DROP TABLE dept"
mycursor.execute(sql)

# insert values

mycursor = mydb.cursor()

sql = "INSERT INTO emp2 (enom, prof, sal, comm ,dno) VALUES (%s, %s, %s, %s, %s)"
val = [
  ('Peter', 'directeur', 10999, 100, 3),
  ('Alex', 'ingénieur', 20999, 100, 4),
  ('Marie', 'dentiste', 20000, 200, 4),
  ('Sam', 'plombier', 9000, 100, 3),
  ('Stéphane', 'ingénieur', 21999, 100, 4),
  ('Will', 'pharmacien', 30999, 70, 4),
  ('Hin', 'ingénieur', 17999, 70, 4),
  ('Bill', 'pharmacien', 9999, 70, 1),
  ('Philippe', 'caméramen', 7500, 300, 5),
  ('Elizabeth', 'développeur', 32500, 30, 5),
  ('marco', 'technicien', 20000, 200, 2)
]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

mycursor = mydb.cursor()

sql = "INSERT INTO dept2 (dnom, dir,ville) VALUES (%s, %s, %s)"
val = [
  ('Commercial', 5, 'Paris'),
  ('Achat', 6, 'Paris'),
  ('RD', 8, 'lyon'),
  ('Qualité', 2, 'Angers'),
  ('Méthode', 7, 'Angers'),

]
mycursor.executemany(sql, val)
mydb.commit()
print(mycursor.rowcount, "was inserted.")

#insert foreign key

mycursor = mydb.cursor()
mycursor.execute("ALTER TABLE emp2 ADD FOREIGN KEY(dno) REFERENCES dept2(dno)")

mycursor = mydb.cursor()
mycursor.execute("ALTER TABLE dept2 ADD FOREIGN KEY(dir) REFERENCES emp2(eno)")


# revenue >10000

mycursor = mydb.cursor()

sql = "SELECT * FROM emp2 WHERE (sal+comm) > 10000"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

# noun and profession of number 10

mycursor = mydb.cursor()

sql = "SELECT enom, prof FROM emp2 WHERE eno=10"

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# noun of employee working in paris
mycursor = mydb.cursor()

sql = 'SELECT \
  emp2.enom AS nom, \
  dept2.ville AS ville \
  FROM emp2 \
  INNER JOIN dept2 ON emp2.dno = dept2.dno\
  where ville="Paris"'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


# noun of commercial department
mycursor = mydb.cursor()

sql = 'SELECT \
  emp2.enom AS nom, \
  dept2.dnom AS department \
  FROM emp2 \
  INNER JOIN dept2 ON emp2.dno = dept2.dno\
  where dnom="Commercial"'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

# profession of director departments
mycursor = mydb.cursor()

sql = 'SELECT \
  emp2.enom AS nom, \
  emp2.prof AS profession, \
  dept2.dnom AS department \
  FROM emp2 \
  INNER JOIN dept2 ON emp2.dno = dept2.dno\
  where dept2.dir=emp2.eno'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)

# noun of engineer directors

mycursor = mydb.cursor()

sql = 'SELECT \
  emp2.enom AS nom, \
  emp2.prof AS profession, \
  dept2.dnom AS department \
  FROM emp2 \
  INNER JOIN dept2 ON emp2.dno = dept2.dno\
  where emp2.prof="ingénieur" AND dept2.dir=emp2.eno'

mycursor.execute(sql)

myresult = mycursor.fetchall()

for x in myresult:
  print(x)


