import mysql.connector

mydb = mysql.connector.connect(
  host="127.0.0.1",
  user="root",
  password="Newstart1234.",
  database="mydatabase"
)

# extract values
import pandas as pd

df = pd.read_csv(r'C:\\Users\sbelli\Documents\CR\souha_talan_academy\module2\act2.6\chain_replacement_clean.csv')

print(df.to_string())
for x in df.index:
  if str(df.loc[(x,"SHIPPING_DATE")]) != "2024" or str(df.loc[(x,"REPLACING-RENAULT-REF")]) == "Empty" or str(df.loc[(x,"REPLACING-SUPPLIER-REF")]) == "Empty" or str(df.loc[(x,"REPLACING-SUPPLIER-NAME")]) == "Empty":
    df.drop(x, inplace=True)

print("data cleaned")
print(df.to_string())

# create new table

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE REPLACING (REPLACING_RENAULT_REF VARCHAR(255), REPLACING_SUPPLIER_REF VARCHAR(255),REPLACING_SUPPLIER_NAME VARCHAR(255))")
print("table created")


# fill the table
mycursor = mydb.cursor()

sql="INSERT INTO REPLACING (REPLACING_RENAULT_REF,REPLACING_SUPPLIER_REF,REPLACING_SUPPLIER_NAME) VALUES(%s,%s,%s)"

for index, row in df.iterrows():
    mycursor.execute(sql, [row["REPLACING-RENAULT-REF"], row["REPLACING-SUPPLIER-REF"], row["REPLACING-SUPPLIER-NAME"]])
mydb.commit()


mycursor.execute("SELECT * FROM REPLACING")
myresult = mycursor.fetchall()
print(myresult)