print("Donner une chaîne")
s = input()


def majusminus(ch):
    chaine = ""
    for i in ch:

        if ord(i) in range(65, 91):  # code ascii des lettres en majuscule
            i = i.lower()
            chaine = chaine + i
        elif ord(i) in range(97, 123):  # code ascii des lettres en minuscule
            i = i.upper()
            chaine = chaine + i
        else:
            chaine = chaine + i

    print(chaine)


majusminus(s)
