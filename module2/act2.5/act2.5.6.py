def palindrome(mot):
    longeur = len(mot)
    n = int(longeur / 2)
    test = True
    i = 0

    while i <= n and test is True:
        for j in range(0, n):
            if mot[j] == mot[longeur - j - 1]:
                test = True
            else:
                test = False
        i += 1
    return test


print("Donner un mot")
mot = input()

if palindrome(mot) is False:
    print("le mot n' est pas un palindrome")
else:
    print("le mot est un palindrome")
