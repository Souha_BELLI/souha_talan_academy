cartedeciel = ["Sirius", "Rigel", "Bételgeuse", "Arcturus", "Aldébaran", "Véga", "Deneb", "Altaïr"]

n = len(cartedeciel)


def triBulle(tab):
    n = len(tab)

    for i in range(n - 1, 0, -1):
        for j in range(0, i):

            if tab[j].upper() > tab[j + 1].upper():
                e = tab[j]
                tab[j] = tab[j + 1]
                tab[j + 1] = e


def dichotomie(t, e):
    a = 0
    b = len(t) - 1
    while a <= b:
        m = (a + b) // 2
        if t[m] == e:
            return True
        if t[m] < e:
            a = m + 1
        else:
            b = m - 1
    return False


triBulle(cartedeciel)
print("La carte de ciel est:")
print(cartedeciel)

# add a star
print("Entrer une nouvelle étoile :")
etoile = input()

cartedeciel.append(etoile)

# keep list sorted

triBulle(cartedeciel)
print("Le tableau des étoiles est :")
print(cartedeciel)

# search for a star
print("Entrer le nom de l'étoile à rechercher :")
etoilerecherchee = input()

if dichotomie(cartedeciel, etoilerecherchee) is True:
    print("L'étoile cherchée existe dans le tableau")
else:
    print("L'étoile cherchée n'existe pas dans le tableau")
